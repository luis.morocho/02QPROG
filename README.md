![](images/ctu-readme-logo.jpg)
![](images/euroteq-readme-logo.jpg)

## 02QPRG-notebooks

These notebooks are the accompanying interactive resources for the course 02QPROG held and the Czech Technical University in Prague. The notebooks are based on the [QWorld](https://qworld.net)'s [bronze-qiskit](https://gitlab.com/qworld/bronze-qiskit) and [silver](https://gitlab.com/qworld/silver) notebooks and have been adjusted to correspond to a one semester university course.

### Prerequisites

These notebooks assume sufficient knowledge about quantum mechanics, elementary linear algebra, and basics of python computing.

### Sections

- Python (for a quick review)
- Basic math (for a quick review)
- Quantum basics
    - Qubit, 
- Quantum systems with Qiskit
    - Qiskit basics: circuit design, visualization, and simulation
    - quantum basics: quantum coin-flipping and Hadamard operator, quantum states and opeators, visualization of a real-valued qubit, superposition and measurements
    - quantum operators on a real-valued single qubit (rotations and reflections) and quantum tomography
    - entanglement and basic quantum protocols superdense coding and quantum teleportation
    - Grover's search algorithm

## Contribution

Please make a pull request or create an issue for _reporting typo_ or _your corrections_.

Please create an issue for _your questions_, _initiating a discussion_, or _proposing a contribution_.


## License

The text and figures are licensed under the Creative Commons Attribution 4.0 International Public License (CC-BY-4.0), available at https://creativecommons.org/licenses/by/4.0/legalcode. 

The code snippets in the notebooks are licensed under Apache License 2.0, available at http://www.apache.org/licenses/LICENSE-2.0.

## Acknowledgements

![](images/qworld/readme-logo.jpg)

02PRG-notebooks were based on [QWorld](https://qworld.net)'s [bronze-qiskit](https://gitlab.com/qworld/bronze-qiskit) and [silver](https://gitlab.com/qworld/silver) notebooks. See the original readme files README-bronze-qiskit.md and README-silver.md for all details.

We acknowledge support from the Czech Technical University in Prague and the EuroTeQ university alliance.

We use [MathJax](https://www.mathjax.org) to display mathematical expressions on html files (e.g., exercises).

We use open source interactive tool [quantumgame](http://play.quantumgame.io) for showing quantum coin flipping experiments.

## Credits

Bronze was created by [Abuzer Yakaryilmaz](http://abu.lu.lv) (QWorld & QLatvia) in October 2018, and it has been developed and maintained by him. 

Özlem Salehi Köken (QWorld & QTurkey) and Maksims Dimitrijevs (QWorld & QLatvia) are the other contributors by preparing new notebooks and revising the existing notebooks.

Bronze was publicly available on July 7, 2019.

Bronze-Qiskit was released by Abuzer Yakaryilmaz in February 2021.

Most of the <font style="color: #A9A9A9;"><b><i>Silver</i></b></font> is prepared under the project QPool2019 [GitLab repository](https://gitlab.com/qkitchen/qpool2019) in 2019-2020 and it is expected to have contributions from public as well. 

We would like to thank participants of the QSilver Revision Week organized in January 2021 and the participants of the QSilver Pilot Workshop organized in April 2021 for revising the material.

See individual notebooks and the readme files for [Bronze-qiskit](README-bronze-qiskit.md) and [Silver](README-silver.md) for credits.

Creator and maintainer of 02QPROG: Aurel Gabris

Contributions to 02QPROG by: Iskender Yalcinkaya, Vlastimil Hudeček

